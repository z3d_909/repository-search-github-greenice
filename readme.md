# Repository search github | Greenice
#### Test task

##### Description

Project is search repositories on github by using github api. All of found repositories
you can add to favorites and remove if them already exists

Execute search can only registered users

Project uses Laravel 6, Vue, Vuex

##### Installation

    git clone https://z3d_909@bitbucket.org/z3d_909/repository-search-github-greenice.git
    
    composer install
    npm install
    
    cp .env.examle .env
    php artisan key:generate
    
    # configure database connection in .env

    php artisan migrate
    
    npm run dev # or for development mode
    npm run prod
    
        

