<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function (){
    Route::get('/favorites',    'FavoriteController@getFavorites');
    Route::post('/favorites',   'FavoriteController@store');
    Route::delete('/favorites', 'FavoriteController@remove');
    Route::post('/favorites/check', 'FavoriteController@checkFavorites');
});




