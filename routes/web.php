<?php

Auth::routes();
Route::get('/',          'HomeController@showSearch')->name('search');
Route::get('/favorites', 'HomeController@showFavorites')->name('favorites');
