@extends('layouts.app')

@section('content')
    <div class="container search">
        <search-form label="{{ __('Search') }}"></search-form>
        <table-repositories></table-repositories>
        <pagination></pagination>
    </div>
@endsection
