@extends('layouts.app')

@section('content')
    <div class="login">

        <form class="form" action="{{ route('login') }}" method="POST">
            <div class="page-title">
                <h3>{{ __('Sign in') }}</h3>
            </div>
            @csrf

            @if(count($errors))
                <div class="error">
                    @foreach($errors->all() as $error)
                        {{ $error }}<br/>
                    @endforeach
                </div>
            @endif

            <div class="form-group">
                <input id="email" name="email" type="text" class="form-control" value="{{ old('email') }}" required>
                <label for="email" class="form-label">{{ __('Your Email') }}</label>
            </div>
            <div class="form-group">
                <input id="password" name="password" type="password" class="form-control" required>
                <label class="form-label">{{ __('Password') }}</label>
            </div>

            <div class="button-group">
                <button class="button is-outlined">{{ __('Login') }}</button>
                <a href="{{ route('password.request') }}" class="button is-link ml-1">{{ __('Forgot password?') }}</a>
                <a href="{{ route('register') }}" class="button is-outlined is-danger ml-auto mr-0">{{ __('Registration') }}</a>
            </div>
        </form>

    </div>
@endsection
