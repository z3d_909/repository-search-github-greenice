@extends('layouts.app')


@section('content')
    <div class="login">

        <form class="form" method="POST" action="{{ route('register') }}">
            <div class="page-title">
                <h3>{{ __('Register') }}</h3>
            </div>
            @csrf

            @if(count($errors))
                <div class="error">
                    @foreach($errors->all() as $error)
                        {{ $error }}<br/>
                    @endforeach
                </div>
            @endif

            <div class="form-group">
                <input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}" required>
                <label for="name" class="form-label">{{ __('Your Name') }}</label>
            </div>
            <div class="form-group">
                <input id="email" name="email" type="text" class="form-control" value="{{ old('email') }}" required >
                <label for="email" class="form-label">{{ __('Your E-mail') }}</label>
            </div>
            <div class="form-group">
                <input id="password" name="password" type="password" class="form-control" required>
                <label for="password" class="form-label">{{ __('Password') }}</label>
            </div>
            <div class="form-group">
                <input id="password-confirm" name="password_confirmation" type="password" class="form-control" required>
                <label for="password-confirm" class="form-label">{{ __('Confirm Password') }}</label>
            </div>

            <div class="button-group">
                <button class="button is-outlined">{{ __('Register') }}</button>
            </div>
        </form>

    </div>
@endsection
