@extends('layouts.app')

@section('content')
    <div class="login">

        <form class="form" action="{{ route('password.update') }}" method="POST">
            <div class="page-title">
                <h3>{{ __('Reset Password') }}</h3>
            </div>
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">

            @if(count($errors))
                <div class="error">
                    @foreach($errors->all() as $error)
                        {{ $error }}<br/>
                    @endforeach
                </div>
            @endif

            <div class="form-group">
                <input id="email" name="email" type="text" class="form-control" value="{{ $email ?? old('email') }}" required>
                <label for="email" class="form-label">{{ __('Your Email') }}</label>
            </div>
            <div class="form-group">
                <input id="password" name="password" type="password" class="form-control" required>
                <label for="password" class="form-label">{{ __('Password') }}</label>
            </div>
            <div class="form-group">
                <input id="password-confirm" name="password_confirmation" type="password" class="form-control" required>
                <label for="password-confirm" class="form-label">{{ __('Confirm Password') }}</label>
            </div>

            <div class="button-group">
                <button class="button is-outlined">{{ __('Reset Password') }}</button>
            </div>
        </form>

    </div>
@endsection
