@extends('layouts.app')

@section('content')
    <div class="login">

        <form class="form" action="{{ route('password.email') }}" method="POST">
            <div class="page-title">
                <h3>{{ __('Reset Password') }}</h3>
            </div>
            @csrf

            @if(count($errors))
                <div class="error">
                    @foreach($errors->all() as $error)
                        {{ $error }}<br/>
                    @endforeach
                </div>
            @endif

            <div class="form-group">
                <input id="email" name="email" type="text" class="form-control" value="{{ old('email') }}" required>
                <label for="email" class="form-label">{{ __('Your Email') }}</label>
            </div>

            <div class="button-group">
                <button class="button is-outlined">{{ __('Send Password Reset Link') }}</button>
            </div>
        </form>

    </div>
@endsection
