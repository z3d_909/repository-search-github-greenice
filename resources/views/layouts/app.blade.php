<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{--CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if (Auth::check())
        <meta name="api-token" content="{{ Auth::user()->api_token }}">
    @endif
    <title>{{config('APP_NAME', 'Repository search github | Greenice')}}</title>

    @section('stylesheet')
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @show
</head>
<body>
<div id="app">
    @if (Auth::check())
        <div class="header">
            <div class="container content">
                <div class="title">
                    <h1>{{config('APP_NAME', 'Repository search github | Greenice')}}</h1>
                </div>
                <div class="controls">
                    <a class="button is-link" href="{{ route('search') }}">{{ __('Search') }}</a>
                    <a class="button is-link" href="{{ route('favorites') }}">{{ __('Favorites') }}</a>
                    <form action="{{ route('logout') }}" method="POST">
                        @csrf
                        <button class="button is-link">{{ __('Logout') }}</button>
                    </form>
                </div>

            </div>
        </div>
    @endif
    @yield('content')
</div>
<script src="{{ mix('js/app.js') }}"></script>

@section('scripts')
    <script>
        const app = new Vue({
            el: '#app',
        });
    </script>
@show

</body>
</html>
