/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('vue2-animate/dist/vue2-animate.min.css');

window.Vue = require('vue');

import Vuex from 'vuex'
import store from './store';
import { Icon, Notification } from 'element-ui';

Vue.use(Vuex);
Vue.use(Icon);

Vue.prototype.$notify = Notification;
Vue.prototype.$store = new Vuex.Store(store);

Vue.component('search-form', require('./components/SearchComponent').default);
Vue.component('table-repositories', require('./components/TableRepositoriesComponent').default);
Vue.component('pagination', require('./components/PaginationComponent').default);
Vue.component('favorite', require('./components/FavoriteComponent').default);

