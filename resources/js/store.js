export default {
    state() {
        return {
            perPage: 15,
            searchResults: {},
            githubUrl: 'https://api.github.com/search/repositories',
            inFavorites: [],
        }
    },
    getters() {},
    mutations: {
        SET_SEARCH_RESULT(state, payload) {
            state.searchResults = payload;
        },

        SET_SHOWMORE(state, payload) {
            state.searchResults.items.push(...payload.items);
            state.searchResults.pages.next    += 1;
            state.searchResults.pages.current += 1;
        },

        /**
         * payload - ids array of searched repositories on github or favorites
         * Execute request to server and check, which ids exiest in our db
         *
         * @param state
         * @param payload
         * @constructor
         */
        SET_CHECK_FAVORITE(state, payload) {
            if (payload.length > 0) {
                axios.post('/api/favorites/check', { data: payload }, {
                    headers: {
                        'Authorization': 'Bearer ' + document.head.querySelector('meta[name="api-token"]').content,
                        'Accept': 'application/json',
                    }
                })
                    .then( response => {
                        state.inFavorites.push(...response.data);
                        state.inFavorites.filter((el, index, arr) => index === arr.indexOf(el));
                    });
            }
        },

        REMOVE_FROM_FAVORITES(state, id) {
            state.inFavorites.splice(state.inFavorites.indexOf(id),1);
        }
    },
    actions() {},
}
