<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Favorite extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'html_url'         => $this->url,
            'owner'            => [ 'login' => $this->owner],
            'description'      => $this->description,
            'stargazers_count' => $this->stargazers,
            'in_favorite'      => true,
            'user'             => new User($this->user),
            'created_at'       => $this->created_at->format('Y.m.d H:i:s'),
        ];
    }
}
