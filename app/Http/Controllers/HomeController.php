<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     * Deny for not authenticated users
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the search form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showSearch()
    {
        return view('home');
    }

    /**
     *  Show the favorite form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showFavorites()
    {
        return view('favorites');
    }
}
