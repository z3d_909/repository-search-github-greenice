<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\Http\Resources\Favorite as FavoriteResource;
use App\Http\Requests\Favorite\RemoveFavorite;
use App\Http\Requests\Favorite\StoreFavorite;
use App\Http\Requests\Favorite\CheckFavorite;

class FavoriteController extends Controller
{
    /**
     * Return collection of Favorite resource
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getFavorites()
    {
        return FavoriteResource::collection(Favorite::with('user')->paginate());
    }

    /**
     * Create new instance of model Favorite after validation
     *
     * @param StoreFavorite $request
     * @return FavoriteResource
     */
    public function store(StoreFavorite $request)
    {
        return new FavoriteResource(Favorite::create($request));
    }

    /**
     * Remove instance of model Favorite after validation
     *
     * @param RemoveFavorite $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(RemoveFavorite $request)
    {
        return response()->json(Favorite::findOrFail($request->id)->remove());
    }

    /**
     * return array with exists ids in favorites
     *
     * @param CheckFavorite $request
     * @return mixed
     */
    public function checkFavorites(CheckFavorite $request) {
        return Favorite::checkFavorites($request->data);
    }
}
