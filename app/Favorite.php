<?php

namespace App;

use App\Http\Requests\Favorite\StoreFavorite;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{

    protected $perPage = 15;

    /**
     * Add new favorite repository in db
     *
     * @param StoreFavorite $request
     * @return Favorite|null
     */
    public static function create(StoreFavorite $request)
    {
        $favorite = new self;
        $favorite->id          = $request->id;
        $favorite->name        = $request->name;
        $favorite->url         = $request->url;
        $favorite->owner       = $request->owner;
        $favorite->description = $request->description;
        $favorite->stargazers  = $request->stargazers;

        $favorite->user()->associate(auth()->user());

        return $favorite->save() ? $favorite : null;
    }

    /**
     * Remove repository
     *
     * @return bool|null
     * @throws \Exception
     */
    public  function remove()
    {
        return $this->delete();
    }

    public static function checkFavorites(array $idsList) {
        return self::whereIn('id', $idsList)->pluck('id');
    }

    /**
     * User relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            'App\User'
        );
    }
}
